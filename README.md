# MKU 1.2 #

* This PHP SDK is to help our partner in Development and Integration into our MKU Service.
* Version 1.2

### How do I get set up? ###

* Clone this Repo.
* update config/zing.php :

```
#!php

<?php

return array(
    'SecretKey' => 'YOUR_SECRET_KEY',
    'StoreId' => 'YOUR_STORE_ID',
    'webRequestUrl' => 'WEB_REQUEST_URL',
    'LogPath' => '/path/to/logs/'
);
```

update libraries/Mkupg.php with you're code.

```
#!php

<?php

    public function reserve() {
        ...
                if ($this->valid_signature) {
                    // Start of You're Code

                    // End of You're Code
                    $response = array(
                        'StoreTrxId' => 'YOUR_TRX_ID',
                        'ResponseCode' => '00',
                        'ResponseMessage' => 'Transaction success',
                    );
                }
        ...
    }

    public function confirm() {
        ...
                if ($this->valid_signature) {
                    // Start of You're Code

                    // End of You're Code
                    $response = array(
                        'StoreTrxId' => 'YOUR_TRX_ID',
                        'ResponseCode' => '00',
                        'ResponseMessage' => 'Transaction success',
                        'SN' => '',
                        'PIN' => ''
                    );
                }
        ...
    }
```


### Contributor ###

* Marsino
* Triputranto