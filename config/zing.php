<?php

return array(
    'SecretKey' => 'YOUR_SECRET_KEY',
    'StoreId' => 'YOUR_STORE_ID',
    'webRequestUrl' => 'WEB_REQUEST_URL',
    'LogPath' => '/path/to/logs/'
);