<?php

include 'PaymentGateway.php';

class Mkupg extends PaymentGateway {

    /**
     * Variable for Reserve Log File
     * @var
     */
    public $log_reserve;

    /**
     * Variable for Confirm Log File
     * @var
     */
    public $log_confirm;

    /**
     * Variable for Web Request Log File
     * @var
     */
    public $log_web_request;

    /**
     * Define HTTP Request
     */
    protected $http_request;

    public function __construct() {
        parent::__construct();

        $this->log_reserve = $this->config['LogPath'] . 'reserve_' . date('Ymd') . '.log';
        $this->log_confirm = $this->config['LogPath'] . 'confirm_' . date('Ymd') . '.log';
        $this->log_web_request = $this->config['LogPath'] . 'web_request_' . date('Ymd') . '.log';
        $this->http_request = file_get_contents("php://input");
    }

    /**
     * Execute Reserve
     *
     * Variable in $request are :
     * $request['Type']
     * $request['StoreId']
     * $request['TrxId']
     * $request['LinkId']
     * $request['MSISDN']
     * $request['SMS']
     * $request['TelcoName']
     * $request['Shortcode']
     * $request['Channel']
     * $request['Reference']
     * $request['ProductId']
     * $request['Timestamp']
     * $request['Signature']
     *
     * @return array|mixed|simpleXMLElement|string
     */
    public function reserve() {
        $response = '';
        if($this->http_request) {
            $request = $this->request($this->http_request);
            $log = '[REQUEST:' . json_encode($request) . ']';
            if ($request['Type'] == "RESERVE") {
                if ($this->valid_signature) {
                    // Start of You're Code

                    // End of You're Code
                    $response = array(
                        'StoreTrxId' => 'YOUR_TRX_ID',
                        'ResponseCode' => '00',
                        'ResponseMessage' => 'Transaction success',
                    );
                } else {
                    $response = array(
                        'ResponseCode' => '03',
                        'ResponseMessage' => 'Invalid signature',
                    );
                }
                $response = $this->response($response);
                $log .= '[RESPONSE:'. json_encode($response) .']';
            }
            error_log($log . "\n", 3, $this->log_reserve);
        }
        return $response;
    }

    /**
     * Execute Confirm
     *
     * Variable in $request are :
     * $request['Type']
     * $request['StoreId']
     * $request['TrxId']
     * $request['StoreTrxId']
     * $request['DeliveryResponse']
     * $request['Timestamp']
     * $request['Signature']
     *
     * @return array|mixed|simpleXMLElement|string
     */
    public function confirm() {
        $response = '';
        if($this->http_request) {
            $request = $this->request($this->http_request);
            $log = '[REQUEST:' . json_encode($request) . ']';
            if ($request['Type'] == "CONFIRM") {
                if ($this->valid_signature) {
                    // Start of You're Code

                    // End of You're Code
                    $response = array(
                        'StoreTrxId' => 'YOUR_TRX_ID',
                        'ResponseCode' => '00',
                        'ResponseMessage' => 'Transaction success',
                        'SN' => '',
                        'PIN' => ''
                    );
                } else {
                    $response = array(
                        'ResponseCode' => '03',
                        'ResponseMessage' => 'Invalid signature',
                    );
                }

                $response = $this->response($response);
                $log .= '[RESPONSE:' . json_encode($response) . ']';

                error_log($log . "\n", 3, $this->log_confirm);
            }
        }
        return $response;
    }

    /**
     * Execute Web Request
     *
     * Variable in $response are:
     *
     * $response['Type']
     * $response['LinkId']
     * $response['ResponseCode']
     * $response['ResponseMessage']
     * $response['Timestamp']
     * $response['Signature']
     *
     * @return array
     */
    public function webRequest($link_id, $msisdn, $productId) {
        $request = array(
            'LinkId' => $link_id,
            'MSISDN' => $msisdn,
            'TelcoName' => 'xl',
            'Reference' => '',
            'ProductId' => $productId
        );
        $log = '[REQUEST:'.json_encode($request).']';

        $response = $this->web_request($request);
        $log .= '[RESPONSE:'.json_encode($response).']';

        error_log($log . "\n", 3, $this->log_web_request);
        return $response;
    }

}