<?php

/**
 * Class PaymentGateway
 * ver 1.2
 * Copyright 2013 PT Media Kreasindo Utama
 */
class PaymentGateway {

    /**
     * Set Config array
     */
    public $config;

    /**
     * Variable to define Signature Valid or Not
     */
    public $valid_signature;

    /**
     * Variable contain request
     */
    private $request = array();

    public function __construct() {
        $baseDir = dirname( dirname(__FILE__) );
        $configDir = $baseDir . '/config/';
        $this->config = include $configDir . 'zing.php';
        if(!$this->config) {
            die('CONFIG ERROR');
        }
    }

    /**
     * Function to parse XML Post Request
     *
     * @param $params
     * @return array
     */
    public function request($params) {
	    $ardata = $this->_parseXML($params);
        $this->request = $ardata;
        $this->valid_signature = $this->_validSignature($ardata);

        return $ardata;
    }

    /**
     * Function to generate XML Response
     *
     * @param array $params
     * @return mixed|simpleXMLElement
     */
    public function response($params=array()) {
        $ardata['Type'] = $this->request['Type'];
        $ardata['StoreId'] = $this->request['StoreId'];
        $ardata['TrxId'] = $this->request['TrxId'];
        $ardata['StoreTrxId'] = $params['StoreTrxId'];

        if($this->request['Type']=='RESERVE') {
            $ardata['Price'] = isset($params['Price']) ? $params['Price']:'';
        }

        $ardata['ResponseCode'] = ($this->valid_signature) ? $params['ResponseCode'] : '03';
        $ardata['ResponseMessage'] = ($this->valid_signature)? $params['ResponseMessage'] : 'Invalid signature';

        if($this->request['Type']=='CONFIRM'){
           $ardata['SN'] = ($params['SN']) ? $params['SN']:'';
           $ardata['PIN'] = ($params['PIN']) ? $params['PIN']:'';
        }

        $ardata['Timestamp'] = date("U");

	    return $this->_constructXML($ardata);
    }

    /**
     * Function to Post XML Web Request
     *
     * @param array $params
     * @return array
     */
    public function web_request($params = array()) {
	    $ardata = array(
            'Type' => 'MO',
            'Channel' => 'WEB',
            'StoreId' => $this->config['StoreId'],
            'LinkId' => $params['LinkId'],
            'MSISDN' => $params['MSISDN'],
            'TelcoName' => $params['TelcoName'],
            'Reference' => $params['Reference'],
            'ProductId' => $params['ProductId'],
            'Timestamp' => date('U')
		);

        $body = $this->_constructXML($ardata);
        $response = $this->_doSubmit($this->config['webRequestUrl'], $body);
        return $this->_parseXML($response);
    }

    /**
     * Function to parse XML to Array
     *
     * @param string $ardata
     * @return array
     */
    private function _parseXML($ardata='') {
	    $arr = array();
        $xml = simplexml_load_string($ardata);
        foreach($xml as $k=>$v) {
            $arr[$k]=(string)$v;
        }

	    return $arr;
    }

    /**
     * Function to check if Signature is Valid
     *
     * @param array $ardata
     * @return bool
     */
    private function _validSignature($ardata = array()) {
	    $sign = $ardata['Signature'];
        unset($ardata['Signature']);

        if($sign == $this->_doSignature($ardata)) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Function to generate Signature
     *
     * @param array $ardata
     * @return string
     */
    private function _doSignature($ardata = array()) {
        return md5(implode('', $ardata) . $this->config['SecretKey']);
    }

    /**
     * Function to construct XML data From Array
     *
     * @param array $ardata
     * @return mixed|simpleXMLElement
     */
    private function _constructXML($ardata = array()) {
        $xml = new simpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Response></Response>");
        foreach($ardata as $k=>$v) {
            $xml->addChild($k,$v);
        }

        $xml->addChild('Signature',$this->_doSignature($ardata));
        $xml = $xml->asXML();
	    return $xml;
    }

    /**
     * Function to POST Web Request
     *
     * @param $url
     * @param $body
     * @return bool|string
     */
    private function _doSubmit($url, $body) {
        $headers = array('Content-Type' => 'text/xml');
	    $data = $body;

	    $cpinfo = parse_url($url);
	    $cphost = $cpinfo['host'];
	    $cpport = (!isset($cpinfo['port']))? '80' : $cpinfo['port'];
	    $path = $cpinfo['path'];
	    if(array_key_exists('query',$cpinfo)) {
		    $urlCP = "$path?".$cpinfo['query'];
	    } else {
		    $urlCP = $path;
	    }

	    $mfp = fsockopen($cphost,$cpport,$errno,$errstr,10);
	    if($mfp) {
            $out  = "POST $urlCP HTTP/1.1\r\n";
            $out .= "Host: $cphost\r\n";
            $out .= "Content-length: " . strlen($data) . "\r\n";
            foreach($headers as $hkey => $hval) {
                $out .= "$hkey: $hval\r\n";
            }
            $out .= "User-Agent: Java/1.4.2_08\r\n";
            $out .= "Connection: close\r\n";
            $out .= "\r\n";
            $out .= $data;

            $buffer = '';
            fwrite($mfp, $out);

            while(!feof($mfp)) {
                $buffer.= fread($mfp, 4096);
            }

            fclose($mfp);
            $contents = preg_split("/\r\n\r\n/",$buffer);
            $head = $this->_parse_http_header($contents[0]);

            if(isset($head['Transfer-Encoding'])) {
               if($head['Transfer-Encoding'] == 'chunked'){
                   $content_body = $this->_decode_chunked($contents[1]);
               } else {
                   $content_body = $contents[1];
               }
            } else {
                $content_body = $contents[1];
            }
            return $content_body;
        } else {
            return false;
        }
    }

    /**
     * Function to parse HTTP Header
     *
     * @param $str
     * @return array
     */
    private function _parse_http_header($str) {
        $lines = explode("\r\n", $str);
        $head  = array(array_shift($lines));
        foreach ($lines as $line) {
            list($key, $val) = explode(':', $line, 2);
            if ($key == 'Set-Cookie') {
                $head['Set-Cookie'][] = trim($val);
            } else {
                $head[$key] = trim($val);
            }
        }
        return $head;
    }

    /**
     * Function to decode CURL Chunk
     *
     * @param $str
     * @return string
     */
    private function _decode_chunked($str) {
        for ($res = ''; !empty($str); $str = trim($str)) {
            $pos = strpos($str, "\r\n");
            $len = hexdec(substr($str, 0, $pos));
            $res.= substr($str, $pos + 2, $len);
            $str = substr($str, $pos + 2 + $len);
        }
        return $res;
    }

}